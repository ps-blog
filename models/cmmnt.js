const mongoose = require('mongoose');

//Schema Setup
const cmmntSchema = new mongoose.Schema({
    text: String,
    author: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        username: String
    }
});

module.exports = new mongoose.model('Cmmnt', cmmntSchema);
