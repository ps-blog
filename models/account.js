const mongoose = require("mongoose");

const AccountSchema = new mongoose.Schema({
    name: String,
    email: String,
    author: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User",
        },
        username: String,
    },
    posts: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "blogPost",
        },
    ],
});

module.exports = new mongoose.model("Account", AccountSchema);