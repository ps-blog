const mongoose = require('mongoose');

//Schema Setup
const blogpostSchema = new mongoose.Schema({
    title: String,
    image: String,
    body: String,
    tag: String,
    author: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        },
        username: String
    },
    comments: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Cmmnt'
        }
    ]
});

module.exports = new mongoose.model('Post', blogpostSchema);
