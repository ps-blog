const Post = require('../models/blogPost'),
    Cmmnt = require('../models/cmmnt'),
    User = require('../models/user');

const middlewareObj = {};

middlewareObj.checkPostOwnership = (req, res, next) => {
    if(req.isAuthenticated()) {
        Post.findById(req.params.pid, (err, foundPost) => {
            if(err) {
                console.log(err);
            } else {
                if(!foundPost) {
                    return res.redirect('back');
                }
                if(foundPost.author.id.equals(req.user._id)) {
                    next();
                } else {
                    res.redirect('back');
                }
            }
        });
    } else {
        res.redirect('/');
    }
}

middlewareObj.checkCmmntOwnership = (req, res, next) => {
    if(req.isAuthenticated()) {
        Cmmnt.findById(req.params.cid, (err, foundCmmnt) => {
            if(err) {
                req.flash('error', 'not found...');
                res.redirect('back');
            } else {
                if(foundCmmnt.author.id.equals(req.user._id)) {
                    next();
                } else {
                    req.flash('error', 'Permission denied!');
                    res.redirect('back');
                }
            }
        });
    } else {
        req.flash('error', 'You need to be logged in to do that!');
        res.redirect('back');
    }
}

middlewareObj.isLoggedIn = (req, res, next) => {
    if (req.isAuthenticated()) {
        return next();
    }
    //req.flash("error", "You need to be logged in to do that!"); // Must come before redirecting
    //res.redirect("/login");
};

module.exports = middlewareObj;
