const express   = require('express'),
    router      = express.Router(),
    Post        = require('../models/blogPost'),
    User        = require('../models/user'),
    Account     = require('../models/account'),
    middleware  = require('../middleware');

//----------------------------------------
//  ROUTES
//---------------------------------------
//
//  Index
router.get('/:blog', (req, res) => {
    User.findOne({ username: req.params.blog }, (err, foundUser) => {
        if(err) {
            console.log(err);
        } else {
            let usr ={
                id: foundUser._id,
                username: foundUser.username,
            };
            Post.find({ author: usr }, (err, posts) => {
                if(err) {
                    console.log(err);
                    res.render('index');
                } else {
                    res.render('blog/blog', { posts: posts, account: foundUser });
                }
            })
        }
    })
});

//  New
router.get('/:blog/new', (req, res) => {
    res.render('blog/new');
});

//Show
router.get('/:blog/:pid', (req, res) => {
    Post.findById(req.params.pid).populate('comments').exec((err, foundPost) => {
        if(err) {
            console.log(err);
        } else {
            res.render('blog/show', { post: foundPost, account: req.params.blog });
        }
    });
});

//Create
router.post('/', middleware.isLoggedIn, (req, res) => {
    let author = {
        id: req.user._id,
        username: req.user.username
    }
    req.body.post.author = author;
    req.body.post.body = req.sanitize(req.body.post.body);
    Post.create(req.body.post, (err, newPost) => {
        if(err) {
            res.render('blog/new');
        } else {
            res.redirect(`/blog/${req.user.username}`);
        }
    });
});

//Edit
router.get('/:blog/:pid/edit', middleware.checkPostOwnership, (req, res) => {
    Post.findById(req.params.pid, (err, foundPost) => {
       if(err) {
           console.log(err);
           res.redirect('back');
       } else {
           res.render('blog/edit', {post: foundPost});
       }
    });
});

//Update
router.put('/:blog/:pid', middleware.checkPostOwnership, (req, res) => {
    req.body.post.body = req.sanitize(req.body.post.body);
    Post.findByIdAndUpdate(req.params.pid, req.body.post, (err, updatedPost) => {
        if(err) {
            console.log(err);
            res.redirect('back');
        } else {
            res.redirect(`/blog/${req.user.username}/${req.params.pid}`)
        }
    });
});

//Destroy
router.delete('/:blog/:pid', middleware.checkPostOwnership, (req, res) => {
    Post.findByIdAndRemove(req.params.pid, (err) => {
        if(err) {
            res.redirect(`/blog/${req.params.blog}`);
        } else {
            res.redirect(`/blog/${req.params.blog}`);
        }
    });
})

module.exports = router;