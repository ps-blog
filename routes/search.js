const express   = require('express'),
    router      = express.Router(),
    Post        = require('../models/blogPost');

//index
router.get('/all', (req, res) => {
    Post.find({}, (err, Posts) => {
        if(err) {
            console.log(err);
            res.redirect('/');
        } else {
            res.render('search/all', { posts: Posts });
        }
    });
});

//Search be title
router.get('/title', (req, res) => {
    Post.find({}, (err, Posts) => {
        if(err) {
            res.redirect('back');
        } else {
            res.render('search/title', { posts: Posts });
        }
    });
});

//Search by tag
router.get('/tag', (req, res) => {
    Post.find({}, (err, Posts) => {
        if(err) {
            res.redirect('back');
        } else {
            res.render('search/byTag', { posts: Posts });
        }
    });
});

module.exports = router;