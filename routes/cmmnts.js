const express   = require('express'),
    router      = express.Router({mergeParams: true}),
    Post        = require('../models/blogPost'),
    Cmmnt       = require('../models/cmmnt'),
    middleware  = require('../middleware');

// Comment routes
//---------------

// New
router.get('/new', middleware.isLoggedIn, (req, res) => {
    Post.findById(req.params.pid, (err, foundPost) => {
        if(err) {
            console.log(err);
        } else {
            res.render('comments/new', { post: foundPost });
        }
    });
});

// Create
router.post ('/', middleware.isLoggedIn, (req, res) => {
    req.body.cmmnt.text = req.sanitize(req.body.cmmnt.text);
    Post.findById(req.params.pid, (err, foundPost) => {
        if(err) {
            console.log(err);
            res.redirect('back');
        } else {
            Cmmnt.create(req.body.cmmnt, (err, comment) => {
                if(err) {
                    console.log(err);
                } else {
                    comment.author.id = req.user._id;
                    comment.author.username = req.user.username;
                    comment.save();
                    foundPost.comments.push(comment);
                    foundPost.save();
                    return res.redirect(`/blog/${req.params.blog}/${req.params.pid}`);
                }
            });
        }
    });
});

//Edit
router.get('/:cid/edit', middleware.checkCmmntOwnership,  (req, res) => {
    Cmmnt.findById(req.params.cid, (err, foundCmmnt) => {
        if(err) {
            res.redirect('back');
        } else {
            res.render('comments/edit', {account: req.params.blog, post_id: req.params.pid, comment: foundCmmnt});
        }
    });
});

//Update
router.put('/:cid', middleware.checkCmmntOwnership, (req, res) => {
    Cmmnt.findByIdAndUpdate(req.params.cid, req.body.cmmnt, (err, updatedCmmnt) => {
        if(err) {
            res.redirect('back');
        } else {
            res.redirect(`/blog/${req.params.blog}/${req.params.pid}`);
        }
    });
});

//Destroy 
router.delete('/:cid', middleware.checkCmmntOwnership, (req, res) => {
    Cmmnt.findByIdAndRemove(req.params.cid, (err) => {
        if(err) {
            res.redirect('back');
        } else {
            res.redirect(`/blog/${req.params.blog}/${req.params.pid}`);
        }
    });
});

module.exports = router;