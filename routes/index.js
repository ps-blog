const express       = require('express'),
        router      = express.Router(),
        User        = require("../models/user"),
        Account     = require("../models/account"),
        passport    = require('passport');

//Index
router.get('/', (req, res) => {
    res.render('index');
});

router.get('/browsers', (req, res) => {
    res.render('mbrowsers');
});

//Auth Routes
router.get('/register', (req, res) => {
    res.render('register');
});

router.post("/register", (req, res) => {
    let newUser = new User({ username: req.body.username });
    User.register(newUser, req.body.password, function (err, user) {
        if (err) {
            //req.flash("error", err.message);
            console.log(err);
            return res.redirect("/register");
        }
        passport.authenticate("local")(req, res, function () {
            Account.create(req.body.account, (err, newAccount) => {
                if (err) {
                    console.log(err);
                } else {
                    newAccount.author.id = user._id;
                    newAccount.author.username = user.username;
                    newAccount.save();
                    //req.flash("success", "Welcome " + user.username);
                    res.redirect("/");
                }
            });
        });
    });
});


router.get('/login', (req, res) => {
    res.render('login');
})

router.post(
    "/login",
    passport.authenticate("local", {
        successRedirect: "/",
        failureRedirect: "/login",
    }),
    (req, res) => {}
);

router.get("/logout", (req, res) => {
    req.logout();
    //req.flash("success", "Logged out");
    res.redirect("/");
});


module.exports = router;
