const bodyParser        = require("body-parser"),
      methodOverride    = require("method-override"),
      expressSanitizer  = require("express-sanitizer"),
      mongoose          = require("mongoose"),
      express           = require("express"),
      flash             = require("connect-flash"),
      passport          = require("passport"),
      LocalStrategy     = require("passport-local"),
      User              = require("./models/user"),
      Account           = require("./models/account"),
      app               = express();

//IMPORT ROUTES
const indexRoutes   = require('./routes/index'),
    blogRoutes      = require('./routes/blog'),
    cmmntRoutes     = require('./routes/cmmnts'),
    searchRoutes    = require('./routes/search');

//SETUP
mongoose.connect("mongodb://USER:PASS@SERVER/DATABASE", { 
    useNewUrlParser: true, 
    useUnifiedTopology: true,
    useFindAndModify: false, 
    useCreateIndex: true 
})
    .then(() => console.log(`Database connected`))
    .catch((err) => console.log(Error, err));
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressSanitizer()); //Must be after body-parser
app.use(methodOverride("_method"));
app.use(flash());

//==========================================================
//                  PASSPORT CONFIG
//==========================================================
app.use(
    require("express-session")({
        secret: "I am gonna be the pirate king",
        resave: false,
        saveUninitialized: false,
    })
);
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());
//----------------------------------------------------------

// Current User & Flash Messages Middleware
app.use((req, res, next) => {
    res.locals.currentUser = req.user;
    //res.locals.error = req.flash("error");
    //res.locals.success = req.flash("success");
    next();
});

//Routes
app.use('/', indexRoutes);
app.use('/register', indexRoutes);
app.use('/login', indexRoutes);
app.use('/logout', indexRoutes);
app.use('/blog', blogRoutes);
app.use('/blog/:blog/:pid/comments', cmmntRoutes);
app.use('/search', searchRoutes);

app.listen(3000, () => {
    console.log("Server started...");
})
